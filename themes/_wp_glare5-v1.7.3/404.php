<?php get_header(); ?>

<div id="main" class="sixteen columns normal group">
	<h2 class="page-title"><?php _e('Not found', 'ci_theme'); ?></h2>

	<div class="two-thirds columns alpha content hyphenate">

		<article <?php post_class('entry'); ?>>
			<p><?php _e( 'Oh, no! The page you requested could not be found. Perhaps searching will help...', 'ci_theme' ); ?></p>
			
			<form role="search" method="get" id="search-body" action="<?php echo esc_url(home_url('/')); ?>">
				<div>
					<input type="text" name="s" id="s-body" value="<?php echo (get_search_query()!="" ? get_search_query() : __('Search', 'ci_theme') ); ?>" size="18" onfocus="if (this.value == '<?php _e('Search', 'ci_theme'); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e('Search', 'ci_theme'); ?>';}" />
					<input type="submit" id="searchsubmit-body" value="Search" />
				</div>
			</form>
			
			<script type="text/javascript">
				// focus on search field after it has loaded
				document.getElementById('s-body') && document.getElementById('s-body').focus();
			</script>
		</article><!-- /entry -->

	</div><!-- two-thirds -->

	<div class="one-third columns omega sidebar">
		<?php dynamic_sidebar('blog-sidebar'); ?>
	</div><!-- /one-third -->
</div><!-- /main -->

<?php get_footer(); ?>