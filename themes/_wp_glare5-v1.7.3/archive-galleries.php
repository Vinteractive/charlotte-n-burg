<?php get_header(); ?>

<div id="main" class="sixteen columns normal group">

	<h2 class="page-title"><?php _e('Galleries Archive', 'ci_theme'); ?></h2>

	<div class="row col-listing">

		<?php 
			if(ci_setting('gallery_listing_cols')!='1-1-1')
			{
				$cols = (int)ci_setting('gallery_listing_cols');
				$parent_cols = 16;
			}
			else
			{
				$cols = 3;
				$parent_cols = '1-1-1';
			}
			ci_column_classes($cols, $parent_cols, true);
		?>
		<?php while ( have_posts() ) : the_post(); ?>
			<article class="<?php echo ci_column_classes($cols, $parent_cols); ?> columns set">

					<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?><a href="<?php the_permalink(); ?>"><?php } ?>
						<?php the_post_thumbnail('gallery_thumb', array('class'=>'scale-with-grid')); ?>
					<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?></a><?php } ?>
						
					<a href="<?php the_permalink(); ?>" class="text-link"><?php the_title(); ?></a>
					
					<?php if (ci_setting('show_gallery_icons') == 'enabled') { ?>
					<div class="zoom">
						<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" rel="pp" class="view-set"><?php _e('View Gallery Cover', 'ci_theme'); ?></a>
						<a href="<?php the_permalink(); ?>" class="view-article"><?php _e('View Gallery', 'ci_theme'); ?></a>
					</div>
					<?php } ?>

			</article><!-- /set -->
		<?php endwhile; ?>

	</div> <!-- /row -->

	<?php ci_pagination(); ?>

</div><!-- /main -->

<?php get_footer(); ?>