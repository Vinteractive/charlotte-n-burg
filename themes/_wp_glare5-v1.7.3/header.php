<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>><!--<![endif]-->

<head>
	<meta charset="utf-8">
	
	<title><?php ci_e_title(); ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<?php // CSS files are loaded via /functions/styles.php ?>

	<?php // JS files are loaded via /functions/scripts.php ?>

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php do_action('after_open_body_tag'); ?>

<?php
	if ( is_page_template('template-home-slider.php') || is_page_template('template-home-video.php') ) :
		$homepage = true;
	else :
		$homepage = false;
	endif;

	if ( !$homepage ) :
		get_template_part('inc_hero');
	else : 
		?><div id="content-wrap"><?php 
	endif; 
?>


<div class="container <?php if ($homepage) echo 'homepage'; ?> wrap">
	<header id="header" class="sixteen columns">
		<div class="row transparent">
			<div class="four columns logo alpha">
				<hgroup class="logogroup <?php logo_class(); ?>">
					<?php ci_e_logo('<h1 id="logo">', '</h1>'); ?>
				</hgroup>
			</div>

			<div class="twelve columns omega">
				<nav id="navigation">
					<?php
						if(has_nav_menu('ci_main_menu'))
							wp_nav_menu( array(
								'theme_location' 	=> 'ci_main_menu',
								'fallback_cb' 		=> '',
								'container' 		=> '',
								'menu_id' 			=> '',
								'menu_class' 		=> 'nav group'
							));
						else
							wp_page_menu(array('menu_class'=>'nav group'));
					?>
				</nav><!-- /navigation -->
			</div><!-- /twelve -->
		</div><!-- /row -->
	</header><!-- /header -->

	<?php if ( $homepage ) : ?>
		</div> <!--/container -->
	<?php endif; ?>