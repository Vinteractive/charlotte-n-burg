</div><!-- /container -->
<div id="footer-wrap">
	<footer class="container footer">
		<div class="sixteen columns">
			<?php dynamic_sidebar('footer-widgets'); ?>
		</div><!-- /sixteen -->
	</footer>
</div><!-- /footer-wrap -->

<b id="t" class="brd"></b> <b id="l" class="brd"></b> <b id="r" class="brd"></b> <b id="b" class="brd"></b>

<?php wp_footer(); ?>

</body>
</html>