<?php
add_action( 'widgets_init', 'ci_widgets_init' );
if( !function_exists('ci_widgets_init') ):
function ci_widgets_init() {

	register_sidebar(array(
		'name' => __( 'Blog Sidebar', 'ci_theme'),
		'id' => 'blog-sidebar',
		'description' => __( 'The list of widgets assigned here will appear in your blog posts.', 'ci_theme'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s group">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title"><span>',
		'after_title' => '</span></h3>'
	));

	register_sidebar(array(
		'name' => __( 'Pages Sidebar', 'ci_theme'),
		'id' => 'pages-sidebar',
		'description' => __( 'The list of widgets assigned here will appear in your pages.', 'ci_theme'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s group">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title"><span>',
		'after_title' => '</span></h3>'
	));

	register_sidebar(array(
		'name' => __( 'Footer Widgets', 'ci_theme'),
		'id' => 'footer-widgets',
		'description' => __( 'The list of widgets assigned here will appear in your footer.', 'ci_theme'),
		'before_widget' => '<aside id="%1$s" class="one-third columns widget %2$s group">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title"><span>',
		'after_title' => '</span></h3>'
	));

	register_sidebar(array(
		'name' => __( 'Contact Page Widgets', 'ci_theme'),
		'id' => 'contact-widgets',
		'description' => __( 'The list of widgets assigned here will appear in your contact page.', 'ci_theme'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s group">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title"><span>',
		'after_title' => '</span></h3>'
	));

}
endif;
?>