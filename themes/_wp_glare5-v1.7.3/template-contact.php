<?php
/*
Template Name: Contact Page
*/
?>
<?php get_header(); ?>


<div id="main" class="sixteen columns normal group">

	<h2 class="page-title"><?php the_title(); ?></h2>

	<div class="row col-listing">

		<?php if ( !ci_setting('disable_map') ): ?>
			<div id="map">map</div><!-- /map -->
		<?php endif; ?>
		<div class="row set-content">

		<?php while ( have_posts() ) : the_post(); ?>
				<div class="two-thirds columns alpha">
					<div class="post-form">
						<?php the_content(); ?>
					</div>
				</div>
		<?php endwhile; ?>

		<div class="one-third columns omega">
			<?php dynamic_sidebar('contact-widgets'); ?>
		</div>
		</div><!-- /row -->

	</div><!-- /row -->

</div><!-- /main -->

<?php if ( !ci_setting('disable_map') ): ?>
	<script>
		jQuery(document).ready(function() {
			initialize();
		});
	</script>
<?php endif; ?>

<?php get_footer(); ?>