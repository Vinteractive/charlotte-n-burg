<?php get_header(); ?>
<?php the_post(); ?>

<div id="main" class="sixteen columns normal group">
	<h2 class="page-title"><?php the_title(); ?></h2>

	<div class="two-thirds columns alpha content hyphenate">

		<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
			<?php ci_the_post_thumbnail(array('class' => 'featured-image scale-with-grid')); ?>
			<?php the_content(); ?>
			<?php wp_link_pages(); ?>
		</article><!-- /post -->

	</div><!-- two-thirds -->

	<div class="one-third columns omega sidebar">
		<?php dynamic_sidebar('pages-sidebar'); ?>
	</div><!-- /one-third -->

</div><!-- /main -->

<?php get_footer(); ?>