<?php get_header(); ?>

<div id="main" class="sixteen columns normal group">
	<h2 class="page-title">
		<?php
			if ( is_home() || is_front_page() ) {
				ci_e_setting('blog_header');
			} else if ( is_date() ) {
				single_month_title(' ');
			} else {
				single_term_title();
			}
		?>
	</h2>

	<div class="two-thirds columns alpha content hyphenate">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('entry listing'); ?>>
				<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permanent Link to: %s', 'ci_theme'), get_the_title())); ?>"><?php the_title(); ?></a></h2>
				
				<p class="meta"><time datetime="<?php echo esc_attr(get_the_date('Y-m-d')); ?>"><?php echo get_the_date(); ?></time> <span>//</span> <?php _e('Author:', 'ci_theme'); ?> <?php the_author_link(); ?> <span>//</span> CATEGORY: <?php the_category(','); ?></p>
				
				<?php the_post_thumbnail('blog-thumb', array('class'=>'featured-image scale-with-grid')); ?>
				
				<?php ci_e_content(); ?>
			</article><!-- /post -->
		<?php endwhile; ?>

		<?php ci_pagination(); ?>

	</div><!-- two-thirds -->

	<div class="one-third columns omega sidebar">
		<?php dynamic_sidebar('blog-sidebar'); ?>
	</div><!-- /one-third -->

</div><!-- /main -->

<?php get_footer(); ?>