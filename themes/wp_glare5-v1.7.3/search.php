<?php get_header(); ?>

<div id="main" class="sixteen columns normal group">
	<h2 class="page-title"><?php _e('Search results', 'ci_theme'); ?></h2>

	<div class="two-thirds columns alpha content hyphenate">

		<article class="entry listing">
			<?php
				global $wp_query;
				
				$found = $wp_query->post_count > $wp_query->found_posts ? $wp_query->post_count : $wp_query->found_posts;
				$none = __('No results found. Please broaden your terms and search again.', 'ci_theme');
				$one = __('Just one result found. We either nailed it, or you might want to broaden your terms and search again.', 'ci_theme');
				$many = sprintf(__("%d results found.", 'ci_theme'), $found);
			?>
			<p><?php ci_e_inflect($found, $none, $one, $many); ?></p>
			
			<?php if($found==0) get_search_form(); ?>
	
		</article><!-- /article -->
	
	
		<?php if (have_posts()): while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('entry listing'); ?>>
				<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permanent Link to: %s', 'ci_theme'), get_the_title())); ?>"><?php the_title(); ?></a></h2>
				
				<p class="meta"><time datetime="<?php echo esc_attr(get_the_date('Y-m-d')); ?>"><?php echo get_the_date(); ?></time> <span>//</span> <?php _e('Author:', 'ci_theme'); ?> <?php the_author_link(); ?> <span>//</span> <a href="<?php comments_link(); ?>"><?php comments_number(__('0 Comments', 'ci_theme'), __('1 Comment', 'ci_theme'), __('% Comments', 'ci_theme')); ?></a></p>
				
				<?php the_post_thumbnail('blog-thumb', array('class'=>'featured-image scale-with-grid')); ?>
				
				<?php the_excerpt(); ?>
			</article><!-- /post -->
		<?php endwhile; endif; ?>

		<?php ci_pagination(); ?>
	
	</div>

	<div class="one-third columns omega sidebar">
		<?php dynamic_sidebar('blog-sidebar'); ?>
	</div><!-- /one-third -->

</div><!-- /main -->

<?php get_footer(); ?>