<?php
/*
Template Name: Homepage with Slider
*/
?>

<?php get_header(); ?> 

<?php the_post(); ?>

	<?php
		$gal_no = ci_setting('homepage_gallery_items');
		$gal_no = !empty($gal_no) ? $gal_no : 4;

		$gallery_front = new WP_Query(array(
			'posts_per_page' => $gal_no,
			'post_type'      => 'galleries',
			'meta_key'       => 'ci_cpt_gallery_homepage',
			'meta_value'     => 'enabled'
		));
	?>

	<div id="controls-wrapper" class="load-item">
		<div id="controls">
			<a id="prevslide" class="btn-slider"><?php _e('Previous', 'ci_theme'); ?></a><a id="pauseplay" class="btn-slider"><?php _e('Play', 'ci_theme'); ?></a><a id="nextslide" class="btn-slider"><?php _e('Next', 'ci_theme'); ?></a><a id="hide-slider" class="btn-slider"><?php _e('Hide Slider', 'ci_theme'); ?></a><a id="show-slider" class="btn-slider"><?php _e('Show Slider', 'ci_theme'); ?></a>
		</div>
	</div><!-- /controls-wrapper -->
	
	<?php
		$homepage_columns_number = ci_setting('homepage_columns_number');
		$homepage_columns_number = !empty($homepage_columns_number) ? $homepage_columns_number : 4;
		$cols_no = "set-" . $homepage_columns_number;
		$home_set = $gallery_front->post_count <= $homepage_columns_number ? "home-set" : "home-set-more" ;
	?>
	
	<div id="<?php echo $home_set; ?>" class="container <?php echo $cols_no; ?>">
		<div class="sixteen columns">

			<?php ci_column_classes($homepage_columns_number, 16, true); ?>
			<?php while ( $gallery_front->have_posts() ) : $gallery_front->the_post(); ?>
				<article class="<?php echo ci_column_classes($homepage_columns_number, 16); ?> columns set">

					<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?><a href="<?php the_permalink(); ?>"><?php } ?>
						<?php the_post_thumbnail('gallery_thumb', array('class'=>'scale-with-grid')); ?>
					<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?></a><?php } ?>

					<a href="<?php the_permalink(); ?>" class="text-link"><?php the_title(); ?></a>

					<?php if (ci_setting('show_gallery_icons') == 'enabled') : ?>
						<div class="zoom">
							<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" rel="pp" class="view-set"><?php _e('View Gallery Cover', 'ci_theme'); ?></a>
							<a href="<?php the_permalink(); ?>" class="view-article"><?php _e('View Gallery', 'ci_theme'); ?></a>
						</div>
					<?php endif; //show_gallery_icons ?>

				</article><!-- /set -->
			<?php endwhile; ?>
	
			<?php wp_reset_postdata(); ?>
	
		</div><!-- /sixteen columns -->
	</div><!-- /container -->

</div><!-- /content-wrap -->

<b id="t" class="brd"></b>
<b id="l" class="brd"></b>
<b id="r" class="brd"></b>
<b id="b" class="brd"></b>

<?php wp_footer(); ?>

<?php if (ci_setting('slider_autoslide') == "enabled" ) { $auto = 1; } else { $auto = 0; } ?>

<?php $home_gallery = ci_featgal_get_attachments(); ?>
<script type="text/javascript">
	jQuery(function($){
		$.supersized({
		
			autoplay			: <?php echo $auto ?>,
			random				: 0,
			slide_interval		: <?php ci_e_setting('slider_speed'); ?>,
			transition			: '<?php ci_e_setting('slider_effect'); ?>',
			transition_speed	: <?php ci_e_setting('slider_duration'); ?>,
			slide_links			: 'blank',
			slides 				: [
				<?php
					if ( $home_gallery->have_posts() ) :
						$total = $home_gallery->post_count;
						$i = 1;
						while ( $home_gallery->have_posts() ) : $home_gallery->the_post();
							if ( $i !== $total ) {
								echo "{image : '". ci_get_image_src($post->ID, 'full')."'},";
							} else {
								echo "{image : '".ci_get_image_src($post->ID, 'full')."'}";
							}
						
							$i++;
					endwhile; endif; wp_reset_postdata();
				?>
			]
		});
	});
</script>

</body>
</html>