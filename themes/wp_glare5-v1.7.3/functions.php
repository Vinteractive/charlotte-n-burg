<?php 

// permalink rewrite
// http://wordpress.stackexchange.com/questions/175434/conditionally-custom-post-type-url-rewrite/175689#175689
add_filter('post_type_link', 'replace_link', 1, 3);
function replace_link( $link, $post = 0 ){
    //custom post type "item"
    if ( $post->post_type == 'galleries' ){
            //registered taxonomy for custom post type "item_category"
            $terms = get_the_terms( $post->ID, 'gallery-category' );

            if ( $terms && ! is_wp_error( $terms ) ) : 
                $links = array();
                //put all taxanomy asigned to that post into one array
                foreach ( $terms as $term ) 
                {
                    $links[] = $term->name;
                }
                $links = str_replace(' ', '-', $links); 

            endif;

            //check if there is particular taxonomy and change URL structure
            if (in_array("Services", $links)) {
                 return home_url('services/'. $post->post_name);            
            }
            if (in_array("Gallery", $links)) {
                return home_url('gallery/'. $post->post_name);
            }

    } else {
        return $link;
    }
}

function custom_rewrite_rule_services() {
   add_rewrite_rule('services/([^/]+)/?$','index.php?galleries=$matches[1]','top');
}
add_action('init', 'custom_rewrite_rule_services');

// function custom_rewrite_rule_galleries() {
//    add_rewrite_rule('gallery/([^/]+)/?$','index.php?galleries=$matches[1]','top');
// }
// add_action('init', 'custom_rewrite_rule_galleries');
// end rewrite





// RAW TEMPLATE \/
	get_template_part('panel/constants');

	load_theme_textdomain( 'ci_theme', get_template_directory() . '/lang' );

	// This is the main options array. Can be accessed as a global in order to reduce function calls.
	$ci = get_option(THEME_OPTIONS);
	$ci_defaults = array();

	// The $content_width needs to be before the inclusion of the rest of the files, as it is used inside of some of them.
	if ( ! isset( $content_width ) ) $content_width = 620;

	//
	// Let's bootstrap the theme.
	//
	get_template_part('panel/bootstrap');

	//
	// Use HTML5 on galleries
	//
	add_theme_support( 'html5', array( 'gallery' ) );

	//
	// Define our various image sizes.
	//
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 300, 150, true );
	add_image_size( 'blog-thumb', 602, 350, true);
	add_image_size( 'ci_page_header', 9999, 400, true);
	add_image_size( 'gallery_thumb', 460, 460, true);
	add_image_size( 'fullsize_thumb', 940, 460, true);


	// Let the theme know that we have WP-PageNavi styled.
	add_ci_theme_support('wp_pagenavi');


	// Let the user choose a color scheme on each post individually.
	add_ci_theme_support('post-color-scheme', array('page', 'post'));


	// Don't use default styles for galleries
	add_filter( 'use_default_gallery_style', '__return_false' );

	// Remove width and height attributes from the <img> tag.
	// Remove also when an image is sent to the editor. When the user resizes the image from the handles, width and height
	// are re-inserted, so expected behaviour is not lost.
	add_filter('post_thumbnail_html', 'ci_remove_thumbnail_dimensions');
	add_filter('image_send_to_editor', 'ci_remove_thumbnail_dimensions');
	if( !function_exists('ci_remove_thumbnail_dimensions') ):
	function ci_remove_thumbnail_dimensions($html)
	{
		$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
		return $html;
	}
	endif;


	add_filter('the_content', 'ci_prettyPhotoRel', 12);
	add_filter('get_comment_text', 'ci_prettyPhotoRel');
	add_filter('wp_get_attachment_link', 'ci_prettyPhotoRel');
	if( !function_exists('ci_prettyPhotoRel') ):
	function ci_prettyPhotoRel($content)
	{
		global $post;
		$pattern = "/<a(.*?)href=('|\")([^>]*).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>(.*?)<\/a>/i";
		
		if(woocommerce_enabled())
			$replacement = '<a$1href=$2$3.$4$5 rel="thumbnails"$6 class="zoom">$7</a>';
		else
			$replacement = '<a$1href=$2$3.$4$5 rel="pp['.$post->ID.']"$6>$7</a>';
		
		$content = preg_replace($pattern, $replacement, $content);
		return $content;
	}
	endif;


	// Responsivize embedded videos
	add_filter('embed_oembed_html', 'ci_embed_oembed_html', 99, 4);
	if ( !function_exists('ci_embed_oembed_html') ):
	function ci_embed_oembed_html($html, $url, $attr, $post_id) {
		return '<div class="video-hold">' . $html . '</div>';
	}
	endif;

?>