<?php get_header(); ?>

<div id="main" class="sixteen columns normal group">
	<h2 class="page-title"><?php ci_e_setting('blog_header'); ?></h2>

	<div class="two-thirds columns alpha content hyphenate">
		<?php while ( have_posts() ) : the_post(); ?>

			<article <?php post_class('entry'); ?>>
				<h2><?php the_title(); ?></h2>

				<p class="meta"><time datetime="<?php echo esc_attr(get_the_date('Y-m-d')); ?>"><?php echo get_the_date(); ?></time> <span>//</span> <?php _e('Author:', 'ci_theme'); ?> <?php the_author_link(); ?> <span>//</span> CATEGORY: <?php the_category(','); ?></p>

				<?php ci_the_post_thumbnail(array('class'=>'featured-image scale-with-grid')); ?>

				<?php the_content(); ?>
				<?php wp_link_pages(); ?>

				<?php if(ci_setting('show_related_posts')=='enabled' and !is_attachment()): ?>
					<div id="related-items">
						<h3 class="widget-title"><span><?php _e('Related Articles', 'ci_theme'); ?></span></h3>
					
						<div class="row">
							<?php
								$term_list = array();
								$terms = get_the_terms(get_the_ID(), 'category');
								if(is_array($terms))
								{
									foreach($terms as $term)
										$term_list[] = $term->slug;
								}
								
								$args = array(
									'post_type' => 'post',
									'posts_per_page' => 2,
									'post_status' => 'publish',
									'post__not_in' => array(get_the_ID()),
									'orderby' => 'rand',
									'tax_query' => array(
										array(
											'taxonomy' => 'category',
											'field' => 'slug',
											'terms' => $term_list
										)
									)
								);
								$related_posts = get_posts($args);
							?>

							<?php ci_column_classes(2, '1-1', true); ?>
							<?php foreach($related_posts as $rpost): ?>
								<article class="<?php echo ci_column_classes(2, '1-1'); ?> columns set">
									<?php
										$attr = array(
											'title' => trim(strip_tags( $rpost->post_title )),
											'class' => 'scale-with-grid'
										);
										echo get_the_post_thumbnail($rpost->ID, 'gallery_thumb', $attr);
									?>
									<a href="<?php echo get_permalink($rpost->ID); ?>"><?php echo get_the_title($rpost->ID); ?> </a>
									<div class="zoom">
										<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($rpost->ID, 'full') ); ?>" class="view-set" rel="pp"><?php _e('View Set', 'ci_theme'); ?></a>
										<a href="<?php echo get_permalink($rpost->ID); ?>" class="view-article"><?php _e('View Article', 'ci_theme'); ?></a>
									</div>
								</article>
							<?php endforeach; ?>
					
							<?php wp_reset_postdata(); ?>
						</div><!-- #similar-portfolios -->
					</div>
				<?php endif; ?>

				<div class="row post-comments">
					<?php comments_template(); ?>
				</div>

			</article><!-- /post -->

		<?php endwhile; ?>

	</div><!-- two-thirds -->

	<div class="one-third columns omega sidebar">
		<?php dynamic_sidebar('blog-sidebar'); ?>
	</div><!-- /one-third -->

</div><!-- /main -->

<?php get_footer(); ?>