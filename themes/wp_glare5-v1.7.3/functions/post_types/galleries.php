<?php
//
// Gallery Post Type related functions.
//
add_action('init', 'ci_create_cpt_gallery');
add_action('admin_init', 'ci_add_cpt_gallery_meta');
add_action('save_post', 'ci_update_cpt_gallery_meta', 10, 2);

if( !function_exists('ci_create_cpt_gallery') ):
function ci_create_cpt_gallery() {
	$labels = array(
		'name' => _x('Galleries', 'post type general name', 'ci_theme'),
		'singular_name' => _x('Galleries', 'post type singular name', 'ci_theme'),
		'add_new' => __('New Gallery', 'ci_theme'),
		'add_new_item' => __('Add New Gallery', 'ci_theme'),
		'edit_item' => __('Edit Gallery', 'ci_theme'),
		'new_item' => __('New Gallery', 'ci_theme'),
		'view_item' => __('View Gallery', 'ci_theme'),
		'search_items' => __('Search Galleries', 'ci_theme'),
		'not_found' =>  __('No Galleries found', 'ci_theme'),
		'not_found_in_trash' => __('No Galleries found in the trash', 'ci_theme'),
		'parent_item_colon' => __('Parent Gallery:', 'ci_theme')
	);
	
	$args = array(
		'labels' => $labels,
		'singular_label' => _x('Gallery', 'post type singular name', 'ci_theme'),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'has_archive' => true,
		'rewrite' => true,
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
		'menu_icon' => 'dashicons-format-gallery'
	);
	
	register_post_type( 'galleries' , $args );
}
endif;

if( !function_exists('ci_add_cpt_gallery_meta') ):
function ci_add_cpt_gallery_meta(){
	add_meta_box("ci_cpt_gallery_meta", __('Gallery Details', 'ci_theme'), "ci_add_cpt_gallery_meta_box", "galleries", "normal", "high");
}
endif;

if( !function_exists('ci_update_cpt_gallery_meta') ):
function ci_update_cpt_gallery_meta($post_id) {
	if ( !ci_can_save_meta('galleries') ) return;

	update_post_meta($post_id, "ci_cpt_gallery_internal_slider", ci_sanitize_checkbox($_POST["ci_cpt_gallery_internal_slider"], 'disabled'));
	update_post_meta($post_id, "ci_cpt_gallery_homepage", ci_sanitize_checkbox($_POST["ci_cpt_gallery_homepage"], 'enabled'));
	update_post_meta($post_id, "ci_cpt_gallery_featured_as_header", ci_sanitize_checkbox($_POST["ci_cpt_gallery_featured_as_header"], 'enabled'));
	ci_metabox_gallery_save($_POST);

}
endif;

if( !function_exists('ci_add_cpt_gallery_meta_box') ):
function ci_add_cpt_gallery_meta_box()
{
	ci_prepare_metabox('galleries');

	?><p><?php _e('You can create a  gallery by pressing the "Add Images" button below. You should also set a featured image that will be used as this Gallery\'s cover.', 'ci_theme'); ?></p><?php

	ci_metabox_gallery();
	ci_metabox_checkbox('ci_cpt_gallery_internal_slider', 'disabled', __('Disable the internal slider for this Gallery (will display the photos in columns instead).', 'ci_theme'));
	ci_metabox_checkbox('ci_cpt_gallery_homepage', 'enabled', __('Show this gallery on the homepage', 'ci_theme'));
	ci_metabox_checkbox('ci_cpt_gallery_featured_as_header', 'enabled', __("Use the Featured Image as this gallery's header image", 'ci_theme'));

}
endif;

?>