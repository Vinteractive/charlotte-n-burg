<?php
//
// Page post type related function
//
add_action('admin_init', 'ci_add_page_video_meta');
add_action('save_post', 'ci_update_page_video_meta');

if( !function_exists('ci_add_page_video_meta') ):
function ci_add_page_video_meta(){
	add_meta_box("ci_video_meta", __('Featured Video', 'ci_theme'), "ci_add_page_video_meta_box", "page", "normal", "high");
	add_meta_box("ci_gallery_meta", __('Gallery Listing Details', 'ci_theme'), "ci_add_page_gallery_meta_box", "page", "normal", "high");
	add_meta_box("ci_home_gallery_meta", __('Homepage Gallery Details', 'ci_theme'), "ci_add_homepage_gallery_meta_box", "page", "normal", "high");
}
endif;

if( !function_exists('ci_update_page_video_meta') ):
function ci_update_page_video_meta($post_id)
{
	if ( !ci_can_save_meta('page') ) return;

	update_post_meta($post_id, "video_url", esc_url_raw($_POST["video_url"]));
	update_post_meta($post_id, "base_galleries_category", intval($_POST["base_galleries_category"]));
	ci_metabox_gallery_save($_POST);

}
endif;

if( !function_exists('ci_add_page_video_meta_box') ):
function ci_add_page_video_meta_box($post)
{
	ci_prepare_metabox('page');

	?><p><?php _e('Paste the URL of the YouTube video you would like to have on your homepage.', 'ci_theme'); ?></p><?php
	ci_metabox_input('video_url', __('YouTube Video URL:', 'ci_theme', array('esc_func' => 'esc_url')));

	ci_bind_metabox_to_page_template('ci_video_meta', 'template-home-video.php', 'tpl_home_video');
}
endif;

if( !function_exists('ci_add_page_gallery_meta_box') ):
function ci_add_page_gallery_meta_box($post)
{
	ci_prepare_metabox('page');

	$category = get_post_meta($post->ID, 'base_galleries_category', true);
	?><p><?php _e('Select the base galleries category. Only items and sub-categories of the selected category will be displayed. If you don\'t select one (i.e. empty) all galleries will be shown. You need to select the <strong>Gallery Listing</strong> template for this option to work.', 'ci_theme'); ?></p><?php
	wp_dropdown_categories(array(
		'selected' => $category,
		'name' => 'base_galleries_category',
		'show_option_none' => ' ',
		'taxonomy' => 'gallery-category',
		'hierarchical' => 1,
		'show_count' => 1,
		'hide_empty' => 0
	));

	ci_bind_metabox_to_page_template('ci_gallery_meta', 'template-galleries.php', 'tpl_galleries');

}
endif;

if ( !function_exists('ci_add_homepage_gallery_meta_box') ):
function ci_add_homepage_gallery_meta_box($post)
{
	ci_prepare_metabox('page');
	ci_metabox_gallery();

	ci_bind_metabox_to_page_template('ci_home_gallery_meta', 'template-home-slider.php', 'tpl_home_gallery');
}
endif;
?>