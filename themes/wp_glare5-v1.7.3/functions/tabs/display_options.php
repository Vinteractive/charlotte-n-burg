<?php global $ci, $ci_defaults, $load_defaults, $content_width; ?>
<?php if ($load_defaults===TRUE): ?>
<?php
	add_filter('ci_panel_tabs', 'ci_add_tab_display_options', 40);
	if( !function_exists('ci_add_tab_display_options') ):
		function ci_add_tab_display_options($tabs) 
		{ 
			$tabs[sanitize_key(basename(__FILE__, '.php'))] = __('Display Options', 'ci_theme'); 
			return $tabs; 
		}
	endif;
	
	// Default values for options go here.
	// $ci_defaults['option_name'] = 'default_value';
	// or
	// load_panel_snippet( 'snippet_name' );

	$ci_defaults['blog_header'] = __('From the blog...', 'ci_theme');
	$ci_defaults['show_gallery_icons'] = 'enabled';
	$ci_defaults['show_related_posts'] = 'enabled';
	$ci_defaults['default_header_bg'] = ''; // Holds the URL of the image file to use as header background
	$ci_defaults['default_header_bg_hidden'] = ''; // Holds the attachment ID of the image file to use as header background

	load_panel_snippet('pagination');
	load_panel_snippet('excerpt');
	load_panel_snippet('seo');
	load_panel_snippet('comments');


	load_panel_snippet('featured_image_single');


	// Set our full width template width.
	add_filter('ci_full_template_width', 'ci_fullwidth_width');
	function ci_fullwidth_width()
	{ 
		return 940; 
	}
	load_panel_snippet('featured_image_fullwidth');


	// Change the Read More markup.
	add_filter('ci-read-more-link', 'ci_theme_readmore', 10, 3);
	function ci_theme_readmore($html, $text, $link)
	{
		//$link = '<a class="ci-read-more" href="'. get_permalink($post_id) . '">' . ci_setting('read_more_text') . '</a>';
		return '<a class="ci-more-link" href="'.esc_url($link).'">'.$text.'</a>';
	}

?>
<?php else: ?>

	<fieldset class="set">
		<p class="guide"><?php _e('Upload or select an image to be used as the default header background on your blog section. This will be displayed only on listing pages and all your single posts. It will also be displayed on all pages if you have not exclusively set a featured image for them. For best results, use a high resolution image, more than 1920 pixels wide.', 'ci_theme'); ?></p>
		<?php ci_panel_upload_image('default_header_bg', __('Upload a header image', 'ci_theme')); ?>
		<input id="default_header_bg_hidden" class="uploaded-id" type="hidden" name="<?php echo THEME_OPTIONS; ?>[default_header_bg_hidden]" value="<?php echo esc_attr($ci['default_header_bg_hidden']); ?>" />
	</fieldset>

	<fieldset class="set">
		<p class="guide"><?php _e('Here you can change the heading that appears on top of blog posts and blog related pages.', 'ci_theme'); ?></p>
		<?php ci_panel_input('blog_header', __('Blog header', 'ci_theme')); ?>
	</fieldset>

	<?php load_panel_snippet('pagination'); ?>	

	<?php load_panel_snippet('excerpt'); ?>	

	<?php load_panel_snippet('seo'); ?>	

	<?php load_panel_snippet('comments'); ?>	

	<?php load_panel_snippet('featured_image_single'); ?>

	<?php load_panel_snippet('featured_image_fullwidth'); ?>

	<fieldset class="set">
		<p class="guide"><?php _e('You can enable or disable the  "Related posts" section that appear on all single posts , just below the content.' , 'ci_theme'); ?></p>
		<?php ci_panel_checkbox('show_related_posts', 'enabled', __('Show Related Posts', 'ci_theme')); ?>
	</fieldset>

	<fieldset class="set">
		<p class="guide"><?php _e('You can enable or disable the "Zoom" & "Link" icons on the homepage and gallery listings.' , 'ci_theme'); ?></p>
		<?php ci_panel_checkbox('show_gallery_icons', 'enabled', __('Show Gallery Icons', 'ci_theme')); ?>
	</fieldset>


<?php endif; ?>