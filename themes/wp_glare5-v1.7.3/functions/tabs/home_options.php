<?php global $ci, $ci_defaults, $load_defaults; ?>
<?php if ($load_defaults===TRUE): ?>
<?php
	add_filter('ci_panel_tabs', 'ci_add_tab_homepage_options', 20);
	if( !function_exists('ci_add_tab_homepage_options') ):
		function ci_add_tab_homepage_options($tabs) 
		{ 
			$tabs[sanitize_key(basename(__FILE__, '.php'))] = __('Homepage Options', 'ci_theme'); 
			return $tabs; 
		}
	endif;

	// Default values for options go here.
	// $ci_defaults['option_name'] = 'default_value';
	// or
	// load_panel_snippet( 'snippet_name' );

	$ci_defaults['homepage_gallery_items'] 	= '4';
	$ci_defaults['homepage_columns_number']	= '4';

	$ci_defaults['slider_autoslide'] 		= '1';
	$ci_defaults['slider_effect'] 			= 'fade';
	$ci_defaults['slider_direction'] 		= 'horizontal';
	$ci_defaults['slider_speed'] 			= 5000;
	$ci_defaults['slider_duration']			= 700;

?>
<?php else: ?>

	<fieldset class="set">
		<p class="guide"><?php _e("The following options control the homepage's number of columns (2,3 or 4) and the number of galleries that will appear on this page." , 'ci_theme'); ?></p>
		<?php ci_panel_input('homepage_gallery_items', __('Number of galleries', 'ci_theme')); ?>
		
		<fieldset class="mt10 mb10">
		<?php
			$columns = array(
				'2' => __('2 Columns', 'ci_theme'),
				'3' => __('3 Columns', 'ci_theme'),
				'4' => __('4 Columns (Default)', 'ci_theme')
			);
			ci_panel_dropdown('homepage_columns_number', $columns, __('Homepage number of columns', 'ci_theme'));
		?>
		</fieldset>
		
		<p class="guide"><?php _e("The following options control the homepage's <strong>supersized slider</strong>. You may enable or disable auto-sliding by checking the appropriate option and further control its behavior." , 'ci_theme'); ?></p>
		
		<?php ci_panel_checkbox('slider_autoslide', 'enabled', __('Enable auto-slide', 'ci_theme')); ?>

		<fieldset class="mt10 mb10">
		<?php
			$options = array(
				'fade' => __('Fade effect (Default)', 'ci_theme'),
				'slideTop' => __('Slide in from top', 'ci_theme'),
				'slideRight' => __('Slide in from the right', 'ci_theme'),
				'slideBottom' => __('Slide in from bottom', 'ci_theme'),
				'slideLeft' => __('Slide in from the left', 'ci_theme'),
				'carouselRight' => __('Carousel from right to left', 'ci_theme'),
				'carouselLeft' => __('Carousel from left to right', 'ci_theme')
			);
			ci_panel_dropdown('slider_effect', $options, __('Slider Effect', 'ci_theme'));
		?>
		</fieldset>
		
		<?php ci_panel_input('slider_speed', __('Slideshow speed in milliseconds (smaller number means faster)', 'ci_theme')); ?>

		<?php ci_panel_input('slider_duration', __('Animation duration in milliseconds (smaller number means faster)', 'ci_theme')); ?>

	</fieldset>
	
<?php endif; ?>