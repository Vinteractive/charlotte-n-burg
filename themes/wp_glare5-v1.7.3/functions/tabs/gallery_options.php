<?php global $ci, $ci_defaults, $load_defaults; ?>
<?php if ($load_defaults===TRUE): ?>
<?php
	add_filter('ci_panel_tabs', 'ci_add_tab_gallery_options', 50);
	if( !function_exists('ci_add_tab_gallery_options') ):
		function ci_add_tab_gallery_options($tabs) 
		{ 
			$tabs[sanitize_key(basename(__FILE__, '.php'))] = __('Gallery Options', 'ci_theme'); 
			return $tabs; 
		}
	endif;

	// Default values for options go here.
	// $ci_defaults['option_name'] = 'default_value';
	// or
	// load_panel_snippet( 'snippet_name' );

	$ci_defaults['gallery_listing_cols'] = '4';
	$ci_defaults['gallery_cols'] = '4';

?>
<?php else: ?>

	<?php 
		$options = array(
			'8' => __('Eight Columns', 'ci_theme'),
			'4' => __('Four Columns', 'ci_theme'),
			'1-1-1' => __('Three Columns', 'ci_theme'),
			'2' => __('Two Columns', 'ci_theme'),
		);
	?>

	<fieldset class="set">
		<p class="guide"><?php _e('Select how many columns you want the Gallery Listing area to be.', 'ci_theme'); ?></p>
		<?php ci_panel_dropdown('gallery_listing_cols', $options, __('Gallery Listing Columns:', 'ci_theme')); ?>
	</fieldset>
	
	<fieldset class="set">
		<p class="guide"><?php _e('Select how many columns you want to display your Gallery photos in each of your Galleries (this applies if you have the slider turned off in the respective gallery).' , 'ci_theme'); ?></p>
		<?php ci_panel_dropdown('gallery_cols', $options, __('Gallery Page Columns:', 'ci_theme')); ?>
	</fieldset>

<?php endif; ?>