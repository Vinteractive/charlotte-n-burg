<?php 
if( !class_exists('CI_LatestPost') ):
class CI_LatestPost extends WP_Widget {

	function CI_LatestPost(){
		$widget_ops = array('description' => __('Displays a number of the latest posts from specific categories', 'ci_theme'));
		$control_ops = array(/*'width' => 300, 'height' => 400*/);
		parent::WP_Widget('ci_latest_post_widget', $name='-= CI Latest Post =-', $widget_ops, $control_ops);
	}


	function widget($args, $instance) {

		extract($args);
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		$post_type = $instance['post_type'];

		echo $before_widget;

		if ($title) echo $before_title . $title . $after_title;

		$q = new WP_Query( array(
			'post_type' => $post_type,
			'orderby' => 'date',
			'order' => 'DESC',
			'posts_per_page' => 1
		));


		while ( $q->have_posts() ) : $q->the_post();
			?>
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<?php the_excerpt(); ?>
			<?php
		endwhile;
		wp_reset_postdata();

		echo $after_widget;


	} // widget


	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = stripslashes($new_instance['title']);
		$instance['post_type'] = stripslashes($new_instance['post_type']);

		return $instance;
	} // save

	function form($instance){

		$instance = wp_parse_args( (array) $instance, array('title'=>'', 'post_type' => 'post'));

		$title = htmlspecialchars($instance['title']);
		$post_type = $instance['post_type'];

		$types = array();
		$args = array(
			'public'   => true
		);		
		$types = get_post_types($args, 'objects');

		unset($types['attachment']);

		echo '<p><label for="'.$this->get_field_id('title').'">' . __('Title', 'ci_theme') . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . esc_attr($title) . '" class="widefat" /></p>';
		echo '<p><label for="'.$this->get_field_id('post_type').'">' . __('Select a post type to display the latest post from', 'ci_theme') . '</label>';
		?>
		<select id="<?php echo $this->get_field_id('post_type'); ?>" name="<?php echo $this->get_field_name('post_type'); ?>">
			<?php foreach($types as $key=>$type): ?>
				<option value="<?php echo esc_attr($key); ?>" <?php selected($post_type, $key); ?>>
					<?php echo $type->labels->name; ?>
				</option>
			<?php endforeach; ?>
		</select></p>
		<?php


	} // form

} // class


register_widget('CI_LatestPost');

endif; // !class_exists
?>