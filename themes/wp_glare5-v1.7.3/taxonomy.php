<?php get_header(); ?>

	<div id="main" class="sixteen columns normal group">

		<h2 class="page-title"><?php single_cat_title(); ?></h2>
		<div class="sixteen fullwidth columns alpha content">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<article class="<?php echo ci_column_classes(4, 16); ?> columns set">
					<?php the_post_thumbnail('gallery_thumb', array('class'=>'scale-with-grid')); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<div class="zoom">
						<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" rel="pp" class="view-set"><?php _e('View Gallery Cover', 'ci_theme'); ?></a>
						<a href="<?php the_permalink(); ?>" class="view-article"><?php _e('View Gallery', 'ci_theme'); ?></a>
					</div>
				</article><!-- /set -->
		<?php endwhile; endif; ?>
		</div>

	</div><!-- /main -->

<?php get_footer(); ?>