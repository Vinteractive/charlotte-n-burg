<?php
/*
Template Name: Services 
template from : template-galleries.php
*/
?>

<?php get_header(); the_post(); ?>

<div id="main" class="sixteen columns normal group">

	<h2 class="page-title"><?php the_title(); ?></h2>
	
	<div class="row col-listing">
		<?php echo do_shortcode("[metaslider id=193]"); ?>
	</div>
	<div class="row col-listing">

		<?php 
			global $post;
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			$base_category = get_post_meta($post->ID, 'base_galleries_category', true);

			$args = array(
				'post_type' => 'galleries',
				'paged'     => $paged
			);
			
			$args_tax = array(
				'tax_query' => array(
					array(
						'taxonomy' => 'gallery-category',
						'field'            => 'id',
						'terms'            => intval( $base_category ),
						'include_children' => true
					)
				)
			);
		
			if(empty($base_category) or $base_category < 1)
				query_posts($args);
			else
				query_posts(array_merge($args, $args_tax));
		?>

		<?php 
			if(ci_setting('gallery_listing_cols')!='1-1-1')
			{
				$cols = (int)ci_setting('gallery_listing_cols');
				$parent_cols = 16;
			}
			else
			{
				$cols = 3;
				$parent_cols = '1-1-1';
			}
			ci_column_classes($cols, $parent_cols, true);
		?>
		<?php while ( have_posts() ) : the_post(); ?>
			<article class="<?php echo ci_column_classes($cols, $parent_cols); ?> columns set">
					
					<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?><a href="<?php the_permalink(); ?>"><?php } ?>
						<?php the_post_thumbnail('gallery_thumb', array('class'=>'scale-with-grid')); ?>
					<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?></a><?php } ?>
						
					<a href="<?php the_permalink(); ?>" class="text-link"><?php the_title(); ?></a>
					
					<?php if (ci_setting('show_gallery_icons') == 'enabled') { ?>
					<div class="zoom">
						<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" rel="pp" class="view-set"><?php _e('View Gallery Cover', 'ci_theme'); ?></a>
						<a href="<?php the_permalink(); ?>" class="view-article"><?php _e('View Gallery', 'ci_theme'); ?></a>
					</div>
					<?php } ?>

			</article><!-- /set -->
		<?php endwhile; ?>
		
	</div> <!-- /row -->

	<div class="row">
		<?php
			$page = get_page_by_path('services');
			echo apply_filters('the_content', $page->post_content);
		?> 
	</div>

	<?php ci_pagination(); ?>
	
	<?php wp_reset_query(); ?>

</div><!-- /main -->

<?php get_footer(); ?>