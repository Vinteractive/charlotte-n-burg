<?php get_header(); ?>

<?php while( have_posts() ): the_post(); ?>
	<div id="main" class="sixteen columns normal group">

		<h2 class="page-title"><?php the_title(); ?></h2>

		<div class="row col-listing">


			<?php if( get_post_meta($post->ID, 'ci_cpt_gallery_internal_slider', true) != 'disabled') : ?>
				<?php $gallery = ci_featgal_get_attachments(); ?>

				<?php if ( $gallery->have_posts() ) : ?>
					<div class="flexslider">
						<ul class="slides">
							<?php
								// Big slides
								while ( $gallery->have_posts() ) : $gallery->the_post();
									echo '<li><img class="scale-with-grid" src="'.ci_get_image_src( $post->ID, 'fullsize_thumb').'" alt="'. get_the_excerpt() .'"></li>';
								endwhile;
							?>
						</ul>
					</div> <!-- .flexslider -->
				<?php endif; wp_reset_postdata(); //if have_posts ?>

			<?php else : // if the user has selected not to show an internal slider ?>
				<?php $gallery = ci_featgal_get_attachments(); ?>

				<ul class="photo-set group">
					<?php
						if(ci_setting('gallery_cols')!='1-1-1')
						{
							$cols = (int)ci_setting('gallery_cols');
							$parent_cols = 16;
						}
						else
						{
							$cols = 3;
							$parent_cols = '1-1-1';
						}
						ci_column_classes($cols, $parent_cols, true);

						while ( $gallery->have_posts() ) : $gallery->the_post();
							echo '<li class="'.ci_column_classes($cols, $parent_cols).' columns"><a href="'.ci_get_image_src($post->ID, 'large').'" rel="prettyPhoto[pp_gal]" title="'.get_the_excerpt().'"><img src="'.ci_get_image_src( $post->ID, 'gallery_thumb').'" class="scale-with-grid" alt="'. get_the_excerpt() .'"></a></li>';
						endwhile; wp_reset_postdata();
					?>
				</ul>

			<?php endif; // if get_get_post_meta internal slider is true ?>
		</div> <!-- /row listing -->

		<?php
			// Related Galleries
			$term_list = array();
			$terms = get_the_terms(get_the_ID(), 'gallery-category');
			if(is_array($terms))
			{
				foreach($terms as $term)
				{
					$term_list[] = $term->slug;
				}
			}

			$term_list = !empty($term_list) ? $term_list : array('');

			$args = array(
				'post_type'      => 'galleries',
				'posts_per_page' => 4,
				'post_status'    => 'publish',
				'post__not_in'   => array($post->ID),
				'orderby'        => 'rand',
				'tax_query'      => array(
					array(
						'taxonomy' => 'gallery-category',
						'field'    => 'slug',
						'terms'    => $term_list
					)
				)
			);

			$similar_g = new WP_Query($args);

			if ( $similar_g->have_posts() ) {
				$content_class = 'eight columns alpha';
			} else {
				$content_class = 'sixteen columns alpha';
			}
		?>

		<div class="row set-content">
			<div class="<?php echo $content_class; ?>">
				<?php the_content(); ?>
			</div>

			<?php if ( $similar_g->have_posts() ) : ?>
				<div class="eight columns omega">
					<div id="related-items">
						<h3 class="widget-title">
							<!-- VS Addition -->
							<?php $category = $term_list[0]; ?>
							<?php if ( $category === "gallery" ) {
								echo "<span>Other Galleries</span>";
							} else if ($category === "services") {
								echo "<span>Other Services</span>";
							};?>
							<!-- / VS addition -->
						</h3>

						<div class="row">
							<?php ci_column_classes(2, 8, true); ?>
							<?php while ( $similar_g->have_posts() ) : $similar_g->the_post(); ?>

								<article class="<?php echo ci_column_classes(2, 8); ?> columns set">

									<?php if (ci_setting('show_gallery_icons') !== 'enabled') : ?>
										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail('gallery_thumb', array('class'=>'scale-with-grid')); ?>
										</a>
									<?php else: ?>
										<?php the_post_thumbnail('gallery_thumb', array('class'=>'scale-with-grid')); ?>
									<?php endif; ?>

									<a class="text-link" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

									<?php if (ci_setting('show_gallery_icons') == 'enabled') : ?>
										<div class="zoom">
											<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" rel="pp" class="view-set"><?php _e('View Gallery Cover', 'ci_theme'); ?></a>
											<a title="<?php _e('Go to gallery', 'ci_theme'); ?>" href="<?php the_permalink(); ?>" class="view-article"><?php _e("View Gallery", 'ci_theme'); ?></a>
										</div>
									<?php endif; // show_gallery_icons ?>
								</article><!-- /set -->

							<?php endwhile; wp_reset_postdata(); ?>
						</div><!-- /row -->
					</div><!-- /related-articles -->

				</div><!-- /eight columns omega -->
			<?php endif; //related articles if ?>
		</div><!-- /row -->

	</div><!-- /main -->
<?php endwhile; ?>

<?php get_footer(); ?>