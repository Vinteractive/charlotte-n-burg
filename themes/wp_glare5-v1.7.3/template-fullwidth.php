<?php
/*
Template Name: Fullwidth
*/
?>
<?php get_header(); ?>

<?php the_post(); ?>

<div id="main" class="sixteen columns normal group">
	<h2 class="page-title"><?php the_title(); ?></h2>

	<div class="sixteen fullwidth columns alpha content hyphenate">
		<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>
			<?php ci_the_post_thumbnail_full(array('class' => 'featured-image scale-with-grid')); ?>
			<?php the_content(); ?>
			<?php wp_link_pages(); ?>
		</article><!-- /post -->
	</div><!-- fullwidth -->

</div><!-- /main -->

<?php get_footer(); ?>