<?php global $ci, $ci_defaults, $load_defaults, $content_width; ?>
<?php if ($load_defaults===TRUE): ?>
<?php

	//$ci_defaults['ci_show_dashboard_rss'] = '';

?>
<?php else: ?>

	<?php /* if( !CI_WHITELABEL && apply_filters('ci_show_dashboard_rss', true) ): ?>
		<fieldset id="ci-panel-site-other" class="set">
			<legend><?php _e('Other options', 'ci_theme'); ?></legend>
			<?php ci_panel_checkbox('ci_show_dashboard_rss', 'off', __('Hide CSSIgniter News Widget from the Dashboard', 'ci_theme')); ?>
		</fieldset>
	<?php endif; */ ?>

<?php endif; ?>