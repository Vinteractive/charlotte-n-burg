<?php
/*
Template Name: Homepage with Video
*/
?>

<?php get_header(); ?>

<?php the_post(); ?>

	<div id="controls-wrapper" class="load-item">
		<div id="controls">
			<a id="pauseplay" class="btn-slider"><?php _e('Play', 'ci_theme'); ?></a><a id="hide-slider" class="btn-slider"><?php _e('Hide Slider', 'ci_theme'); ?></a><a id="show-slider" class="btn-slider"><?php _e('Show Slider', 'ci_theme'); ?></a>
		</div>
	</div><!-- /controls-wrapper -->
	
	<div id="home-set" class="container">
		<div class="sixteen columns">
	
			<?php 
				$gallery_front = new WP_Query(array(
					'posts_per_page' => 4,
					'post_type' => 'galleries',
					'meta_key' => 'ci_cpt_gallery_homepage',
					'meta_value' => 'enabled'
				));
			?>
			<?php ci_column_classes(4, 16, true); ?>
			<?php while ( $gallery_front->have_posts() ) : $gallery_front->the_post(); ?>
				<article class="<?php echo ci_column_classes(4, 16); ?> columns set">
					<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?><a href="<?php the_permalink(); ?>"><?php } ?>
						<?php the_post_thumbnail('gallery_thumb', array('class'=>'scale-with-grid')); ?>
						<?php if (ci_setting('show_gallery_icons') !== 'enabled') { ?></a><?php } ?>
					<a href="<?php the_permalink(); ?>" class="text-link"><?php the_title(); ?></a>

					<?php if (ci_setting('show_gallery_icons') == 'enabled') : ?>
					<div class="zoom">
						<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" rel="pp" class="view-set"><?php _e('View Gallery Cover', 'ci_theme'); ?></a>
						<a href="<?php the_permalink(); ?>" class="view-article"><?php _e('View Gallery', 'ci_theme'); ?></a>
					</div>
					<?php endif; //show_gallery_icons ?>
				</article><!-- /set -->
			<?php endwhile; ?>

			<?php wp_reset_postdata(); ?>
	
		</div><!-- /sixteen columns -->
	</div><!-- /container -->
</div><!-- /content-wrap -->

<div id="video-wrapper"></div>

<b id="t" class="brd"></b>
<b id="l" class="brd"></b>
<b id="r" class="brd"></b>
<b id="b" class="brd"></b>

<?php wp_footer(); ?>

<?php
	$video_url = get_post_meta($post->ID, 'video_url', true);
	parse_str( parse_url( $video_url, PHP_URL_QUERY ), $vars );
?>
<script type="text/javascript">
	jQuery(function($){
		$('body').tubular('<?php echo $vars['v']; ?>','video-wrapper');
		$('#pauseplay').click(function(){
			if ($(this).hasClass('sz-play')) {
				callPlayer('ytapiplayer','playVideo');
				$(this).removeClass('sz-play');
			} else {
				callPlayer('ytapiplayer','pauseVideo');
				$(this).addClass('sz-play');
			}
		});
	});
</script>

</body>
</html>