jQuery.fn.center = function(pos) {
	var parent = window;
	this.css({
		'position': 'absolute',
		'top': (jQuery(parent).height() - pos + 'px'),
		'left': (((jQuery(parent).width() - this.width()) / 2))
	});
	return this;
}

jQuery(function($){
	//Submenu
	$('.nav').superfish({
		animation: {
			opacity: 'show'
		},
		speed: 'fast',
		delay: 500
	});
	
	var hs = jQuery('#home-set').length > 0 ? jQuery('#home-set') : jQuery('#home-set-more');
	
	//Hide Slider
	$('#hide-slider').click(function(){
		$('#t').fadeOut('fast', function(){
			$('#l').fadeOut('fast', function(){
				$('#b').fadeOut('fast',function(){
					$('#r').fadeOut('fast', function(){
						$('#header').fadeOut('fast', function(){
							hs.fadeOut('fast');	
						});
					});
				});
				
			});
		});
		$(this).removeClass('show').hide();
		$('#show-slider').addClass('show');
		return false;
	});
	
	//Show Slider
	$('#show-slider').click(function(){
		$('#t').fadeIn('fast', function(){
			$('#l').fadeIn('fast', function(){
				$('#b').fadeIn('fast',function(){
					$('#r').fadeIn('fast', function(){
						$('#header').fadeIn('fast', function(){
							hs.fadeIn('fast');	
						});
					});
				});
				
			});
		});
		$(this).removeClass('show').hide();
		$('#hide-slider').addClass('show');
		return false;
	});
	
	// Responsive Menu
	// Create the dropdown base
	$('<select />').appendTo('#navigation');

	// Create default option 'Go to...'
	$('<option />', {
		'selected': 'selected',
		'value'   : '',
		'text'	: 'Go to...'
	}).appendTo('#navigation select');

	// Populate dropdown with menu items
	$('nav a').each(function() {
	var el = $(this);
	$('<option />', {
		'value'   : el.attr('href'),
		'text'	: el.text()
	}).appendTo('nav select');
	});

	$('#navigation select').change(function() {
		window.location = $(this).find('option:selected').val();
	});
			
	// position control bar and sets
	positionControls();
	$(window).resize(function(e){
		positionControls();		
	});
	
	//prettyPhoto
	if ($('a[rel^="prettyPhoto"]').length>0) {
		$('a[rel^="prettyPhoto"]').prettyPhoto({
			deeplinking: false
		});
	}

	if ($('a[rel="pp"]').length>0) {
		$('a[rel="pp"]').prettyPhoto({
			deeplinking: false
		});
	}

	if ($('.gallery a[rel^="thumbnails"]').length>0) {
		$('.gallery a[rel^="thumbnails"]').prettyPhoto({
			deeplinking: false
		});
	}
	
	// Responsive Videos
	if ( $(".content").length ) {
		$(".content").fitVids();
	}
});

jQuery(window).load(function() {
	if ( jQuery('.flexslider').length > 0 ) {
		jQuery('.flexslider').flexslider();
	}
	Hyphenator.run();
});

function positionControls(){
	var hs = jQuery('#home-set');	
	var cw = jQuery('#controls-wrapper');
	
	// Position the homepage galleries
	if (hs.length > 0) {	
		if ( jQuery('.set-2').length > 0 ) {		
			hs.center(585); 
		}
		else if ( jQuery('.set-3').length > 0 ) {
			hs.center(425); 
		}
		else {
			hs.center(345); 
		}		
		cw.center(66);	
	}
	else {
		cw.addClass('controls-wrapper-more');
	}
}

function initialize() {
	var myLatlng = new google.maps.LatLng(ThemeOption.map_coords_lat, ThemeOption.map_coords_long);

	var mapOptions = {
		zoom: parseInt(ThemeOption.map_zoom_level),
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	var map = new google.maps.Map(document.getElementById('map'), mapOptions);
	
	var contentString = '<div id="content">'+ThemeOption.map_tooltip+'</div>';
	
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});

	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		title: ''
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});
}