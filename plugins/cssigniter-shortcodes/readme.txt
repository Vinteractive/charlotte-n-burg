=== CSSIgniter Shortcodes ===
Contributors: anastis, silencerius, tsiger
Plugin Name: CSSIgniter Shortcodes
Plugin URI: http://www.cssigniter.com/ignite/ci-shortcodes/
Author URI: http://www.cssigniter.com/
Author: The CSSigniter Team
Tags: shortcode, shortcodes, button, box, tooltip, separator, blockquote, list, gmaps, google maps, icons
Requires at least: 3.7
Tested up to: 3.9.1
Stable tag: 1.2.1

This plugin defines and allows you to use a lot of useful shortcodes. Need a button? Sure. A message box? You know we have it.

== Description ==

This shortcodes plugin, has been created to complement and be used mainly with CSSIgniter's premium and free themes. But of course, anyone can use it with any theme. 
A lot of useful shortcodes are defined. See the plugin's homepage for a complete guide. And keep in mind that the plugin supports themes. If you don't like the colors, layout, etc, you can easily create you own shortcodes theme using CSS. And don't forget to share your theme with us, so everyone can benefit and see your skills. 

== Installation ==

1. Upload the folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. In the Settings page you will see the "CSSIgniter Shortcodes" settings screen of the plugin.

== Frequently Asked Questions ==

= What shortcodes are supported? =

Quite a few! Check http://www.cssigniter.com/ignite/ci-shortcodes/ for complete usage instructions.

== Screenshots ==

1. Some default-styled shortcodes.

== Changelog ==

= 1.2.2 =
* Added a "Settings" link in the plugins listing page.
* If exists, the Upgrade Notice is now shown in the plugins listing page.

= 1.2.1 =
* Fixed an issue where the columns shortcodes wouldn't get styled appropriately, when compatibility mode was enabled.

= 1.2 =
* Added internationalization support.
* Stylesheets are now loaded on the top of the page.

= 1.1 = 
* Added a missing clearfix class "group" to all stylesheets.

= 1.0 = 
* Initial Release

== Upgrade Notice ==

= 1.2.1 =
Fixes columns output in compatibility mode.
