<?php
/*
Plugin Name: CSSIgniter Shortcodes
Plugin URI: http://www.cssigniter.com/ignite/ci-shortcodes/
Description: Registers a lot of useful shortcodes
Version: 1.2.2
Author: The CSSIgniter Team
Author URI: http://www.cssigniter.com
License: GPL2
*/
?>
<?php
/*	Copyright 2012  CSSIgniter  (email : info@cssigniter.com)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2, as 
	published by the Free Software Foundation.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
?>
<?php 

if (!defined('CI_SHORTCODES_VERSION'))
	define('CI_SHORTCODES_VERSION', '1.2');

if (!defined('CI_SHORTCODES_PLUGIN_INSTALLED'))
	define('CI_SHORTCODES_PLUGIN_INSTALLED', 'cssigniter_shortcodes_plugin_version');

if(!defined('CI_SHORTCODES_BASENAME'))
	define('CI_SHORTCODES_BASENAME', plugin_basename(__FILE__));


load_plugin_textdomain('cishort', false, dirname(plugin_basename(__FILE__)).'/languages/');

require_once('panel.php');

$cishort_options = array(); // Hold the plugin options


global $ci_add_shortcode_js; 	// Boolean. When TRUE, JS will be enqueued. Call ci_shortcodes_add_js(); to set.


// Loads options from DB. Needs to run from within a hook.
add_action('init', 'ci_shortcodes_load_options');
function ci_shortcodes_load_options()
{
	global $cishort_options;
	$cishort_options = get_option(CI_SHORTCODES_PLUGIN_OPTIONS);

	// Activation/Deactivation hooks don't run for all sites in Multisite.
	// Therefore, we'll have to check on each pageload if the options array is actually set.
	// It's not much of an overhead, since we are loading the options anyway.
	if(empty($cishort_options))
		ci_shortcodes_activate();
}


//
// No code. Prevents execution of shortcodes. Useful for tutorials.
//
function ci_shortcodes_nocode( $atts, $content = "" ) {
	return $content;
}


//
// Buttons
//
function ci_shortcodes_buttons( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"color" => "grey",
		"type" => "normal",
		"url" => "#"
	), $atts));

	$output = '<a href="'. $url .'" class="' . ci_shortcodes_class() . ' ci-button '. $color.' '. $type. '"><span><b>'.do_shortcode($content).'</span></b></a> ';
	return $output;
}

//
// Tooltips
//
function ci_shortcodes_tooltips( $atts, $content = null ) {
	ci_shortcodes_add_js();

	extract(shortcode_atts(array(
		"color" => "grey",
		"contents" => ''
	), $atts));
	
	$output = '<span title="'. $contents .'" class="' . ci_shortcodes_class() . ' ci-tooltip '. $color .'">'.do_shortcode($content).'</span>';
	return $output;
}

//
// Boxes
//
function ci_shortcodes_boxes( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"type" => 'normal',
	), $atts));

	$output = '<div class="' . ci_shortcodes_class() . ' group ci-box '.$type.'"><span class="icon"></span><div class="ci-box-content">'.do_shortcode($content).'</div></div>';
	return $output;
}

//
// Special Quotes
//
function ci_shortcodes_quotes( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"type" => 'normal',
		"title" => '',
		"float" => 'none'
	), $atts));
	
	$output = '<blockquote  class="ci-align'.$float.' '. ci_shortcodes_class() . ' ci-blockquote '.$type.'"><div class="ci-blockquote-content"><span>'.$title.'</span>'.do_shortcode($content).'</div></blockquote>';
	return $output;
}


//
// Separator
//
function ci_shortcodes_hr( $atts, $content = null ) {
	extract(shortcode_atts(array(
		"type" => 'normal'
	), $atts));
	
	$output = '<div class="' . ci_shortcodes_class() . ' ci-separator '.$type.'"><span></span></div>';
	return $output;
}

//
// Lists
//

function ci_shortcodes_lists( $atts, $content=null ) {
	extract(shortcode_atts(array(
		"type" => 'normal'
	), $atts));

  $output = '<div class="ci-list ci-list-'.$type.'">'.$content.'</div>';
  return $output;
}

//
// Columns
//
function ci_shortcodes_columns( $atts, $content = null, $name='' ) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => ''
	), $atts));

	$id = (!empty($id)) ? " id='" . esc_attr( $id ) . "'" : '';
	$class = (!empty($class)) ? ' ' . esc_attr( $class ) : '';

	$pos = strpos($name,'_last');

	if ($pos)
		$name = str_replace('_last',' last',$name);

	//$output = "<div{$id} class='ci-column {$name}{$class}'>{$content}</div>";
	$output = '<div'.$id.' class="' . ci_shortcodes_class() . ' ci-column '.$name.$class.'">'.ci_shortcodes_format_content($content).'</div>';
	
	if ($pos)
		$output .= "<div class='clear'></div>";

	return $output;
}

//
//Google Maps Shortcode
//
function ci_shortcodes_googlemaps($atts, $content = null) {
	global $content_width;
	extract(shortcode_atts(array(
		"width" => ((isset($content_width) and !empty($content_width)) ? $content_width : '640'),
		"height" => ((isset($content_width) and !empty($content_width)) ? (480 / 640 * $content_width) : '480'),
		"float" => 'none',
		"src" => ''
	), $atts));
	return '<iframe class="ci-map ci-align'.$float.'" width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$src.'&amp;output=embed"></iframe>';
}


//
// Register our shortcodes with or without a prefix, according to user setting.
//
add_action('wp_loaded', 'ci_shortcodes_register_shortcodes');
function ci_shortcodes_register_shortcodes()
{
	global $cishort_options;
	$short_prefix = $cishort_options['compatibility']=='enabled' ? 'ci-' : '';
	add_shortcode($short_prefix.'nocode', 			'ci_shortcodes_nocode');
	add_shortcode($short_prefix.'button', 			'ci_shortcodes_buttons');
	add_shortcode($short_prefix.'tooltip', 			'ci_shortcodes_tooltips');
	add_shortcode($short_prefix.'box', 				'ci_shortcodes_boxes');
	add_shortcode($short_prefix.'quote', 			'ci_shortcodes_quotes');
	add_shortcode($short_prefix.'one_half', 		'ci_shortcodes_columns');
	add_shortcode($short_prefix.'one_half_last', 	'ci_shortcodes_columns');
	add_shortcode($short_prefix.'one_third', 		'ci_shortcodes_columns');
	add_shortcode($short_prefix.'one_third_last', 	'ci_shortcodes_columns');
	add_shortcode($short_prefix.'two_thirds', 		'ci_shortcodes_columns');
	add_shortcode($short_prefix.'two_thirds_last', 	'ci_shortcodes_columns');
	add_shortcode($short_prefix.'googlemap', 		'ci_shortcodes_googlemaps');
	add_shortcode($short_prefix.'separator', 		'ci_shortcodes_hr');
	add_shortcode($short_prefix.'list', 			'ci_shortcodes_lists');
}



// Register our scripts.
add_action('init', 'ci_register_shortcode_scripts');
function ci_register_shortcode_scripts() {
	wp_register_script('ci_shortcodes_js', plugins_url('ci-shortcodes.js', __FILE__), array('jquery'), CI_SHORTCODES_VERSION, true);
}

// Load the shortcodes' Javascript only if we have a shortcode that needs it on page
add_action('wp_footer', 'ci_print_shortcode_scripts');
function ci_print_shortcode_scripts() {
	global $ci_add_shortcode_js;

	if ( $ci_add_shortcode_js == true )
		wp_print_scripts('ci_shortcodes_js');

}

// Load the appropriate CSS files, according to user setting.
add_action('wp_enqueue_scripts', 'ci_shortcode_load_styles');
function ci_shortcode_load_styles()
{
	$plugin_url = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)) ;
	$rel_path = 'themes/'.ci_shortcodes_theme().'/css';
	$theme_url = $plugin_url . $rel_path;
	
	$path = dirname(__FILE__).'/'.$rel_path;
	if ($handle = opendir($path)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				$file_info = pathinfo($path.'/'.$file);
				if($file_info['extension']=='css')
				{
					wp_enqueue_style('ci-shortcodes-style-'.$file_info['filename'], $theme_url.'/'.$file_info['basename'], array(), CI_SHORTCODES_VERSION);
				}
			}
		}
		closedir($handle);
	}
	
}

function ci_shortcodes_format_content($content)
{
	$content = wptexturize($content);
	$content = convert_smilies($content);
	$content = convert_chars($content);
	$content = wpautop($content);
	$content = shortcode_unautop($content);
	$content = do_shortcode($content);
	return $content;
}

// Call this when the js file needs to be loaded.
function ci_shortcodes_add_js($add=true) { global $ci_add_shortcode_js; $ci_add_shortcode_js = $add; }

// Returns a string suitable as a CSS class name
function ci_shortcodes_class(){ return 'ci-shortcodes-'.ci_shortcodes_theme(); }

// Returns the selected theme name.
function ci_shortcodes_theme() { global $cishort_options; return $cishort_options['theme']; }

// Checks if a shortcode is already defined.
function ci_is_shortcode_defined($shortcode)
{
	global $shortcode_tags;
	if(isset($shortcode_tags[$shortcode]))
		return TRUE;
	else
		return FALSE;
}


add_filter('plugin_action_links_'.CI_SHORTCODES_BASENAME, 'ci_shortcodes_plugin_action_links');
if( !function_exists('ci_shortcodes_plugin_action_links') ):
function ci_shortcodes_plugin_action_links($links) {
	$url = admin_url( 'options-general.php?page=cssigniter_shortcodes_plugin' );
	array_unshift( $links, '<a href="' . esc_url( $url ) . '">' . __( 'Settings', 'cisiw' ) . '</a>' );
	return $links;
}
endif;

add_action('in_plugin_update_message-'.CI_SHORTCODES_BASENAME, 'ci_shortcodes_plugin_update_message', 10, 2);
if( !function_exists('ci_shortcodes_plugin_update_message') ):
function ci_shortcodes_plugin_update_message($plugin_data, $r) {
	if ( !empty( $r->upgrade_notice ) ) {
		printf( '<p style="margin: 3px 0 0 0; border-top: 1px solid #ddd; padding-top: 3px">%s</p>', $r->upgrade_notice );
	}
}
endif;



//
// Handle activation / deactivation
//
register_activation_hook( __FILE__, 'ci_shortcodes_activate' );
function ci_shortcodes_activate()
{
	global $cishort_options;
	$cishort_options = get_option(CI_SHORTCODES_PLUGIN_OPTIONS);
	if ( $cishort_options===false )
	{
		$cishort_options['theme'] = 'default';
		$cishort_options['compatibility'] = 'no';
		update_option( CI_SHORTCODES_PLUGIN_OPTIONS, $cishort_options );
	}
}
register_deactivation_hook( __FILE__, 'ci_shortcodes_deactivate' );
function ci_shortcodes_deactivate()
{
	delete_option(CI_SHORTCODES_PLUGIN_OPTIONS);
	unregister_setting( 'ci_shortcodes_plugin_settings', CI_SHORTCODES_PLUGIN_OPTIONS);
	delete_option(CI_SHORTCODES_PLUGIN_INSTALLED);
}


//
// Handle upgrades
//
$ci_shortcodes_installed_version = get_option(CI_SHORTCODES_PLUGIN_INSTALLED);
if ( empty($ci_shortcodes_installed_version) or $ci_shortcodes_installed_version != CI_SHORTCODES_VERSION )
{
	// Do upgrade stuff.
	_ci_shortcodes_do_upgrade($ci_shortcodes_installed_version);
}

function _ci_shortcodes_do_upgrade($version)
{
	$version = _ci_shortcodes_upgrade_to_1_0($version);		
	$version = _ci_shortcodes_upgrade_to_1_1($version);		
	$version = _ci_shortcodes_upgrade_to_1_2($version);		
	//$version = _ci_shortcodes_upgrade_to_1_2($version);
	update_option(CI_SHORTCODES_PLUGIN_INSTALLED, $version);
}

function _ci_shortcodes_upgrade_to_1_0($version)
{
	if (empty($version) or $version === FALSE)
	{
		// No DB changes in this update
		return '1.0';
	}
	else
	{
		return $version;
	}
}

function _ci_shortcodes_upgrade_to_1_1($version)
{
	if ($version == '0.9' or $version == '1.0')
	{
		// No DB changes in this update
		return '1.1';
	}
	else
	{
		return $version;
	}
}

function _ci_shortcodes_upgrade_to_1_2($version)
{
	if ($version == '1.1')
	{
		// No DB changes in this update
		return '1.2';
	}
	else
	{
		return $version;
	}
}


?>