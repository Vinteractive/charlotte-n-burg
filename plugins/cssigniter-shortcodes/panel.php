<?php
// Build the admin panel

if (!defined('CI_SHORTCODES_PLUGIN_OPTIONS'))
	define('CI_SHORTCODES_PLUGIN_OPTIONS', 'cssigniter_shortcodes_plugin');

if ( is_admin() ){
	add_action('admin_menu', 'ci_shortcodes_menu');
	add_action('admin_init', 'register_ci_shortcode_settings' );
}

function register_ci_shortcode_settings() { // whitelist options
 	register_setting( 'ci_shortcodes_plugin_settings', CI_SHORTCODES_PLUGIN_OPTIONS, 'ci_shortcodes_settings_validate');
}


function ci_shortcodes_menu() {
	add_options_page('CSSIgniter Shortcodes Options', 'CSSIgniter Shortcodes', 'manage_options', CI_SHORTCODES_PLUGIN_OPTIONS, 'ci_shortcodes_plugin_options');
}

// Returns an array of all dir names within the themes directory.
function ci_shortcodes_get_theme_dirs()
{
	$themes = array();
	
	$path = dirname(__FILE__).'/themes/';

	if ($handle = opendir($path)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") { 
				$file_info = pathinfo($path.'/'.$file);
				if ( is_dir($file_info['dirname'].'/'.$file_info['filename']) )
					$themes[] = $file;
			}
		}
		closedir($handle);
	}
	
	return $themes;
}



function ci_shortcodes_settings_validate($settings)
{
	$settings['theme'] = (!isset($settings['theme'])) ? 'default' : str_replace("/", "", $settings['theme']) ;
	$settings['compatibility'] = isset($settings['compatibility']) && $settings['compatibility']=="enabled" ? 'enabled' : "no" ;
	return $settings;
}

function ci_shortcodes_plugin_options() {
	
	if (!current_user_can('manage_options'))  {
		wp_die( __('You do not have sufficient permissions to access this page.', 'cishort') );
	}
	?>
	<div class="wrap">
		<h2><?php _e('CSSIgniter Shortcodes - Settings', 'cishort'); ?></h2>
		<form method="post" action="options.php">
			<?php settings_fields('ci_shortcodes_plugin_settings'); ?>
	
			<table class="form-table">
				<tr valign="top">
					<th scope="row"><?php _e('Theme name', 'cishort'); ?></th>
					<td>
						<?php 
							$cishort_options = get_option(CI_SHORTCODES_PLUGIN_OPTIONS); 
							$dirs = ci_shortcodes_get_theme_dirs(); 
						?>
						<select name="<?php echo CI_SHORTCODES_PLUGIN_OPTIONS; ?>[theme]">
							
							<?php foreach ($dirs as $dir): ?>
								<option value="<?php echo $dir; ?>" <?php selected($dir, $cishort_options['theme']); ?> ><?php echo $dir; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e('Compatibility Mode', 'cishort'); ?></th>
					<td>
						<fieldset>
							<label for="compatibility">
								<input 	type="checkbox" 
										value="enabled" 
										id="compatibility" 
										name="<?php echo CI_SHORTCODES_PLUGIN_OPTIONS; ?>[compatibility]"
										<?php checked('enabled', $cishort_options['compatibility']); ?> />
								<?php _e('Enable Compatibility Mode?', 'cishort'); ?>
							</label>
						</fieldset>
						<span class="description"><?php _e('When this box is checked, all shortcodes registered by this plugin, will need a "ci-" prefix. For example, <strong>[button]</strong> becomes <strong>[ci-button]</strong>, etc.', 'cishort'); ?></span>
					</td>					
				</tr>
			</table>
	
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes', 'cishort') ?>" />
			</p>
		</form>
	</div>
	<?php
}
?>
